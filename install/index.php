<?

use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Localization\LanguageTable;
use Bitrix\Main\SiteTable;
use Bitrix\Currency\CurrencyClassifier;

Loc::loadMessages(__FILE__);

class currency_cdr extends CModule
{
    var $MODULE_ID = 'currency_cdr';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = 'Y';
    var $errors = false;

    function __construct()
    {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (!empty($arModuleVersion['VERSION'])) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = Loc::getMessage("CURRENCY_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("CURRENCY_INSTALL_DESCRIPTION");
    }

    function DoInstall()
    {
        global $APPLICATION;

        $this->InstallFiles();
        $this->InstallDB();
        $this->InstallEvents();

        $GLOBALS["errors"] = $this->errors;
    }

    function DoUninstall()
    {
        global $APPLICATION, $step;

        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();
    }

    function InstallDB()
    {
        global $DB, $APPLICATION;

        $this->errors = false;

        // check db
        if (!$DB->Query("SELECT COUNT(COURSE) FROM currency_cdr", true)) {
            // run db script
            $this->errors = $DB->RunSQLBatch("./db/mysql/install.sql");
        }

        // if we have errors we must make throw
        if ($this->errors !== false) {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

        ModuleManager::registerModule('currency_cdr');

        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $DB, $APPLICATION;

        $this->errors = false;

        $this->errors = $DB->RunSQLBatch("./db/mysql/uninstall.sql");
        if ($this->errors !== false) {
            $APPLICATION->ThrowException(implode('', $this->errors));
            return false;
        }

        CAgent::RemoveModuleAgents('currency_cdr');

        ModuleManager::unRegisterModule('currency_cdr');

        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
        // install components
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/currency/install/components", $_SERVER["DOCUMENT_ROOT"] . "/local/components/currency_cdr", true, true);

        return true;
    }

    function UnInstallFiles()
    {
        // uninstall components
        DeleteDirFilesEx("/local/components/currency_cdr/");

        return true;
    }

    protected function installCurrencies()
    {
        if (!Loader::includeModule('currency_cdr'))
            return;

        $checkDate = Main\Type\DateTime::createFromTimestamp(strtotime('tomorrow 00:01:00'));
        //TODO  add function to agent
        CAgent::AddAgent('\Bitrix\Currency\CurrencyManager::currencyBaseRateAgent();', 'currency', 'Y', 86400, '', 'Y', $checkDate->toString(), 100, false, true);
        unset($checkDate);
    }
}